'use strict';

angular.module('mod.m53')
.controller('howCtrl', ['$scope', 'UserService', '$stateParams', function($scope, UserService, $stateParams){

    $scope.message = "Hello from you module Dashboard";

    $scope.ifJava = false;
    $scope.ifNode = false;
    $scope.ifPhp = false;
    $scope.ifAjs = false;
    $scope.ifRuby = false;
    
    var init = function(){
        var param = $stateParams.technology;

        if( param == "java"){
            $scope.ifJava = true;
        } else  if (param == "php"){
            $scope.ifPhp = true;
        } else {
            $scope.ifNode = true;
        }
    };

    init();
}]);