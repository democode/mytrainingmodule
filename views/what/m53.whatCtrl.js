'use strict';

angular.module('mod.m53')
.controller('whatCtrl', ['$scope', 'UserService', '$state', function($scope, UserService, $state){

    $scope.message = "Hello from you module Dashboard";

    $scope.navigate = function(tech){
        $state.go('m53.how', {technology : tech});
    };
    
    var init = function(){
        
    };

    init();
}]);