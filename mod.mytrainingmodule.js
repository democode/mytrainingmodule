angular.module('mod.m53', []);

angular.module('mod.m53')
  .constant('MOD_mytrainingmodule', {
        API_URL: '',
        API_URL_DEV: ''
    })
	.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {

        $stateProvider
            .state('m53', {
                url: '/mytrainingmodule',
                parent: 'layout',
                template: "<div ui-view></div>",
                data: {
                    type: 'home'
                }
            })
            .state('m53.header', {
               url: '',
               template: "<div></div>",
               data: {
                   templateUrl:'mod_mytrainingmodule/views/menu/mytrainingmodule.header.html',
                   position: 1,
                   type: 'header',
                   name: "Profile",
                   module: "mytrainingmodule"
               }
           })
            .state('m53.dashboard', {
                url: '/dashboard',
                templateUrl: "mod_mytrainingmodule/views/dashboard/mytrainingmodule.dashboard.html",
                data: {
                    type: 'home',
                    menu: true,
                    name: "Dashboard",
                    module: "mytrainingmodule"
                }

            })
            .state('m53.what', {
                url: '/what',
                templateUrl: "mod_mytrainingmodule/views/what/m53.what.html",
                data: {
                    type: 'home',
                    menu: true,
                    name: "What",
                    module: "mytrainingmodule"
                }

            })
            .state('m53.how', {
                url: '/how/:technology',
                templateUrl: "mod_mytrainingmodule/views/how/m53.how.html",
                data: {
                    type: 'home',
                    menu: true,
                    name: "How",
                    module: "mytrainingmodule"
                }

            })

            //FOR APP ADMIN
           .state('m53.admin', {
               url: '/admin',
               templateUrl: "mod_mytrainingmodule/views/admin/mytrainingmodule.admin.html",
               data: {
                   type: 'home',
                   menu: true,
                   admin : true,
                   disabled : false,
                   name: "Admin Page",
                   module: "mytrainingmodule"
               }
           })
           

    }])